import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Window 2.15
import org.kde.kirigami 2.15 as Kirigami
import QtQuick.Layouts 1.15

Window {
    width: 600
    height: 350

    maximumHeight: height
    maximumWidth: width

    minimumHeight: height
    minimumWidth: width

    property string qtVersion
    property string kfVersion
    property string pamacVersion

    title: qsTr("About")
    SystemPalette {
        id: palette
    }

    Rectangle {
        anchors.fill: parent
        color: palette.window
    }
    Row {
        anchors {
            top: parent.top
            left: parent.left
            margins: 20
        }
        spacing: 10

        Image {
            height: 50
            width: height
            id: mcpLogo
            source: "qrc:/icon.svg"
            sourceSize.width: width
            sourceSize.height: height
        }
        Label {
            anchors.verticalCenter: parent.verticalCenter
            font.family: "Comfortaa"
            font.pixelSize: mcpLogo.height-10
            text: "mcp"
        }
    }

    Item {
        anchors {
            bottom: parent.bottom
            horizontalCenter: parent.horizontalCenter
            margins: 10
        }
        height: childrenRect.height
        width: childrenRect.width
        ColumnLayout {
            id: logoLayout
            Image {
                id: logo
                height: 20
                source: "qrc:/logo.svg"
                sourceSize.height: height
                fillMode: Image.PreserveAspectFit
                Layout.alignment: Qt.AlignHCenter
            }
            Label {
                font.family: "Comfortaa"
                verticalAlignment: Qt.AlignVCenter|Qt.AlignHCenter
                text: "enjoy the simplicity"
            }
        }
        MouseArea {
            anchors.fill: logoLayout
            cursorShape: Qt.PointingHandCursor
            onClicked: {
                Qt.openUrlExternally("https://manjaro.org/")
            }
        }
    }




    ColumnLayout {
        id: details
        width: parent.width*0.65
        anchors.margins: 10
        anchors.left: parent.left
        anchors.verticalCenter: parent.verticalCenter

        Label {
            Layout.fillWidth: true
            textFormat: Label.MarkdownText
            wrapMode: Label.WrapAtWordBoundaryOrAnywhere
            text: qsTr("*Manjaro Control Panel* ‒ a control center for Manjaro OS maintainance.")
        }

        RowLayout {
            Layout.fillWidth: true
            spacing: 10
            ColumnLayout {
                Layout.fillWidth: true
                Layout.alignment: Qt.AlignTop
                Label {
                    font.pointSize: 12

                    text: qsTr("Authors")
                }

                Repeater {
                    model: ["**Artem Grinev** ‒ developer", "**Bogdan Covaciu** ‒ icon, design ideas", "**Mark Wagie** ‒ QA"]

                    Label {
                        textFormat: Label.MarkdownText
                        font.pointSize: 8
                        text: modelData
                    }
                }
            }
            ColumnLayout {
                Layout.alignment: Qt.AlignTop|Qt.AlignRight
                Label {
                    font.pointSize: 12

                    text: "Libraries"
                }

                Repeater {
                    model: ["**Qt** "+qtVersion, "**KDE Frameworks** "+kfVersion, "**Kirigami 2**", "**libpamac** "+pamacVersion]

                    Label {
                        textFormat: Label.MarkdownText
                        font.pointSize: 8
                        text: modelData
                    }
                }
            }
        }

        RowLayout {
            ToolButton {
                icon.source: "qrc:/gitlab-logo-500.svg"
                text: qsTr("Contibute")
                onClicked: Qt.openUrlExternally("https://gitlab.com/LordTermor/msm-ng")
            }
            ToolButton {
                icon.name: "license"
                text: "GNU GPLv3"
                onClicked: Qt.openUrlExternally("https://www.gnu.org/licenses/gpl-3.0.en.html")
            }
        }
    }
    Image {
        transform: Scale{
            id: transform
            origin.x: mascot.width/2
        }

        SequentialAnimation {
            id: flipAnimation

            NumberAnimation {
                target: transform
                easing.type: Easing.InOutQuad
                property: "xScale"
                to: -1
                duration: 200
            }
            NumberAnimation {
                easing.type: Easing.InOutQuad
                target: transform
                property: "xScale"
                to: 1
                duration: 300
            }
            NumberAnimation {
                target: transform
                easing.type: Easing.InOutQuad
                property: "xScale"
                to: -1
                duration: 400
            }
            NumberAnimation {
                easing.type: Easing.InOutQuad
                target: transform
                property: "xScale"
                to: 1
                duration: 500
            }

        }
        id: mascot
        source: "qrc:/mascot.svg"
        anchors.right: parent.right
        anchors.left: details.right
        anchors.margins: parent.width*0.07
        anchors.verticalCenter: parent.verticalCenter
        sourceSize.width: width
        fillMode: Image.PreserveAspectFit
        MouseArea {
            anchors.fill: parent
            onClicked: flipAnimation.start()

        }
    }
}
