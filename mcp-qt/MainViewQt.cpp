#include "MainViewQt.h"
#include "ui_MainViewQt.h"
#include <QQmlEngine>
#include <QQuickItem>
#include <QQuickView>
#include <QQuickWidget>
#include <QToolButton>

#include <AbstractViewQt.h>

#include <KCoreAddons>
#include <pamac.h>

MainViewQt::MainViewQt()
    : QMainWindow()
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->quickWidget->setSource(QUrl("qrc:/MainSideMenu.qml"));
    ui->quickWidget->setResizeMode(QQuickWidget::SizeRootObjectToView);
    setWindowTitle("Manjaro Control Panel");

    connect(ui->quickWidget->rootObject(), SIGNAL(itemClicked(int)), this, SLOT(changeItem(int)));
    connect(this, &MainViewQt::itemChanged, [this](int index) {
        QMetaObject::invokeMethod(ui->quickWidget->rootObject(), "highlightItem", Q_ARG(QVariant, index));
    });

    setVersions();
}

void MainViewQt::setVersions()
{
    auto about = ui->quickWidget->rootObject()->findChild<QObject *>("about");

    about->setProperty("pamacVersion", QString::fromUtf8(pamac_get_version()));
    about->setProperty("qtVersion", QString(qVersion()));
    about->setProperty("kfVersion", KCoreAddons::versionString());
}

void MainViewQt::changeItem(int index)
{
    ui->stackedWidget->setCurrentIndex(index);

    Q_EMIT itemChanged(index);
}

void MainViewQt::addView(AbstractView *view, const MetaInfo &info)
{
    QVariantMap object;

    object["name"] = QString::fromStdString(info.name);
    object["description"] = QString::fromStdString(info.description);
    object["iconName"] = QString::fromStdString(info.iconName);
    int index = 0;
    if (auto widget = dynamic_cast<QWidget *>(view)) {
        index = ui->stackedWidget->addWidget(widget);
    } else if (auto qquick = dynamic_cast<MCP::Qt::AbstractViewQML *>(view)) {
        auto wgt = new QQuickWidget();
        wgt->setResizeMode(QQuickWidget::SizeRootObjectToView);
        wgt->setContent(qquick->component()->url(), qquick->component(), qquick);
        index = ui->stackedWidget->addWidget(wgt);
    }

    object["stackIndex"] = index;

    auto list = ui->quickWidget->rootObject()->property("modules").toList();
    list.append(object);
    ui->quickWidget->rootObject()->setProperty("modules", list);

    if (list.size() == 1) {
        changeItem(index);
    }
}

void MainViewQt::_show()
{
    show();
}
