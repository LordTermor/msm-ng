#ifndef MAINVIEWQT_H
#define MAINVIEWQT_H
#include "MainView.h"
#include <QMainWindow>
namespace Ui
{
class MainWindow;
}
class MainViewQt : public QMainWindow, public MainView
{
    Q_OBJECT
public:
    MainViewQt();

protected:
    Ui::MainWindow *ui;

private:
    void setVersions();

public Q_SLOTS:
    void changeItem(int index);

Q_SIGNALS:
    void itemChanged(int index);

    // MainView interface
public:
    virtual void addView(AbstractView *view, const MetaInfo &info) override;
    virtual void _show() override;
};

#endif // MAINVIEWQT_H
