import QtQuick 2.6
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.2
import org.kde.kirigami 2.15 as Kirigami

Kirigami.Card {
    banner {
        source: model.image
    }

    width: 70
    height:70
}
