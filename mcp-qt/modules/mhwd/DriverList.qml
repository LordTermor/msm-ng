import QtQuick 2.15
import QtQuick.Controls 2.15 as Controls
import org.kde.kirigami 2.15 as Kirigami
import QtQuick.Layouts 1.15

ListView {
    interactive: false
    model: ListModel{
        ListElement{
            text: "video-nvidia"
            isFree: false
        }
        ListElement{
            text: "video-linux"
            isFree: true
        }
        ListElement{
            text: "video-modesetting"
            isFree: true
        }
    }

    delegate: Kirigami.BasicListItem{

        contentItem: RowLayout{
            Kirigami.Icon{
                source: model.isFree?"https://upload.wikimedia.org/wikipedia/commons/8/8b/Copyleft.svg":"https://upload.wikimedia.org/wikipedia/commons/b/b0/Copyright.svg"
                Layout.preferredHeight: parent.height
            }

            Controls.Label {
                Layout.fillWidth: true
                text: model.text

            }
            Controls.Button{
                flat: true
                Layout.preferredHeight: parent.height
                icon.name: "download"
            }
        }

    }

}
