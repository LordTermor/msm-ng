import QtQuick 2.15
import QtQuick.Controls 2.15 as Controls
import org.kde.kirigami 2.15 as Kirigami
import QtQuick.Layouts 1.15

ListView {

    header: ColumnLayout{
        width: parent.width

        Kirigami.ListSectionHeader {
            width: parent.width
            text: "Available drivers"
        }
        Kirigami.CardsListView {
            orientation: Qt.Horizontal
            height: 100
            Layout.alignment: Qt.AlignCenter
                        Layout.minimumWidth: 30 * 5 + 40
            model: ListModel{
                ListElement{
                    image: "qrc:/gpu.png"
                }
            }

            delegate: ConfigItem{
                anchors.verticalCenter: parent.verticalCenter
            }
        }
    }
    Component {
        id: delegateComponent
        Kirigami.BasicListItem {
            id: listItem
            contentItem: RowLayout {
                Controls.Label {
                    Layout.fillWidth: true
                    height: Math.max(implicitHeight, Kirigami.Units.iconSizes.smallMedium)
                    text: model.title
                    color: listItem.checked || (listItem.pressed && !listItem.checked && !listItem.sectionDelegate) ? listItem.activeTextColor : listItem.textColor
                }
            }
            onClicked: {
                root.pageStack.push(Qt.resolvedUrl("qrc:/DetailsPage.qml"))
            }
        }
    }

    id: mainList
    Timer {
        id: refreshRequestTimer
        interval: 3000
        onTriggered: page.refreshing = false
    }
    model: ListModel {
        id: listModel

        Component.onCompleted: {

            for (let i = 0; i < 2; ++i) {
                listModel.append({"title": "Drive #" + (i+1),
                                     "sec": "Storage"
                                 });
            }

            listModel.append({"title": "Network card",
                                 "sec": "Network controller"
                             });
            listModel.append({"title": "Memory",
                                 "sec": "Memory controller"
                             });
        }
    }
    moveDisplaced: Transition {
        YAnimator {
            duration: Kirigami.Units.longDuration
            easing.type: Easing.InOutQuad
        }
    }
    delegate: Kirigami.DelegateRecycler {
        width: parent ? parent.width : implicitWidth
        sourceComponent: delegateComponent
    }
    section {
        property: "sec"
        delegate: Kirigami.ListSectionHeader {
            text: section
        }
    }
}
