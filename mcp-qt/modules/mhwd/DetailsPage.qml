import QtQuick 2.6
import QtQuick.Controls 2.15 as Controls
import QtQuick.Layouts 1.2
import org.kde.kirigami 2.15 as Kirigami

Kirigami.ScrollablePage {
    title: "Nvidia GeForce GTX 1660 Ti"
    actions{
        main: Kirigami.Action {
            iconName: "download"
            text: qsTr("Auto-install")
        }
    }

    padding: 0
    Kirigami.Theme.inherit: false
    Kirigami.Theme.colorSet: Kirigami.Theme.View


     ColumnLayout{

         Kirigami.ListSectionHeader{
             text: "Drivers"
         }

        DriverList{
            Layout.fillWidth: true
            Layout.preferredHeight: contentItem.height
        }

        Kirigami.ListSectionHeader{
            text: "Details"
        }

        Kirigami.Page{
            Layout.fillHeight: true

            Controls.Label{
                text: "Device information goes here"
            }
        }
    }
}
