/**
 * SPDX-FileCopyrightText: Year Author <author@domanin.com>
 * SPDX-License-Identifier: GPL-2.0-or-later
 */

#pragma once

#include <KQuickAddons/ManagedConfigModule>
#include <LanguagePackViewQt.h>
#include <QPixmap>
#include <modules/lang/LanguagePacksPagePresenter.h>

class LanguagePackModel : public KQuickAddons::ManagedConfigModule
{
    Q_OBJECT
    Q_PROPERTY(LanguagePackViewQt *view READ view WRITE setView NOTIFY viewChanged)

public:
    LanguagePackModel(QObject *parent, const KPluginMetaData &data, const QVariantList &args);
    virtual ~LanguagePackModel() override;
    // ConfigModule interface
    LanguagePackViewQt *view() const;
    void setView(LanguagePackViewQt *newView);

public Q_SLOTS:
    virtual void load() override;

Q_SIGNALS:
    void viewChanged();

private:
    LanguagePacksPagePresenter *m_presenter;
    LanguagePackViewQt *m_view;
};
