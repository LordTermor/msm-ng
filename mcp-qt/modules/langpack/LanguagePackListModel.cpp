#include "LanguagePackListModel.h"
#include <QMetaEnum>
#include <QUrl>
#include <QtDebug>

#include <algorithm>

LanguagePackListModel::LanguagePackListModel(QObject *parent)
    : QAbstractListModel(parent)
{
}

const std::vector<LanguagePackage> &LanguagePackListModel::list() const
{
    return m_list;
}

void LanguagePackListModel::setList(const std::vector<LanguagePackage> &newList)
{
    if (m_list == newList)
        return;
    beginResetModel();
    m_list = newList;

    std::partition(m_list.begin(), m_list.end(), [](const auto &lhs) {
        return !lhs.installed;
    });
    endResetModel();
    Q_EMIT listChanged();
}

int LanguagePackListModel::rowCount(const QModelIndex &parent) const
{
    return m_list.size();
}

QVariant LanguagePackListModel::data(const QModelIndex &index, int role) const
{
    auto &langPack = m_list[index.row()];
    switch (role) {
    case Name:
        return QString::fromStdString(langPack.packageName);
    case Description:
        return QString::fromStdString(langPack.description);
    case Language:
        return QString::fromStdString(langPack.language);
    case Territory:
        return QString::fromStdString(langPack.territory);
    case IsNeeded:
        return langPack.needed;
    case IsInstalled:
        return langPack.installed;
    case Parents: {
        QStringList parents;

        parents.reserve(langPack.parentPackages.size());

        std::transform(langPack.parentPackages.begin(), langPack.parentPackages.end(), std::back_inserter(parents), QString::fromStdString);

        return parents;
    }
    case FlagSource: {
        auto path = langPack.flagPath();
        if (path.empty()) {
            return QUrl();
        }
        return "file:/" + QString::fromStdString(path);
    }
    }

    return QVariant::Invalid;
}

QHash<int, QByteArray> LanguagePackListModel::roleNames() const
{
    QHash<int, QByteArray> result;
    auto enumeration = QMetaEnum::fromType<Role>();
    for (int i = 0; i < enumeration.keyCount(); i++) {
        result[i + Qt::UserRole + 1] = enumeration.key(i);
        result[i + Qt::UserRole + 1][0] = std::tolower(result[i + Qt::UserRole + 1][0]);
    }
    return result;
}
