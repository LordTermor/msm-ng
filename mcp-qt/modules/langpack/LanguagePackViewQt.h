#ifndef LANGUAGEPACKVIEWQT_H
#define LANGUAGEPACKVIEWQT_H
#include <iostream>

#include <QQuickItem>

#include "LanguagePackListModel.h"
#include "modules/lang/LanguagePackAbstractView.h"

#include <AbstractViewQt.h>

class LanguagePackViewQt : public MCP::Qt::AbstractViewQML, public LanguagePackAbstractView
{
    Q_OBJECT
    QML_ELEMENT
    Q_PROPERTY(LanguagePackListModel *model READ model WRITE setModel NOTIFY modelChanged)

public:
    LanguagePackViewQt();

private:
    LanguagePackListModel *m_model;

Q_SIGNALS:
    void installMissingPackages();
    void requestPackageList();

    // AbstractView interface
    void modelChanged();

public:
    virtual void _show() override;
    virtual void _init() override;

    // LanguagePackAbstractView interface
public:
    virtual void setPackageList(const std::vector<LanguagePackage> &) override;
    virtual void setProgress(const std::string &action, const std::string &status, double progress) override;
    LanguagePackListModel *model() const;
    void setModel(LanguagePackListModel *newModel);
};

#endif // LANGUAGEPACKVIEWQT_H
