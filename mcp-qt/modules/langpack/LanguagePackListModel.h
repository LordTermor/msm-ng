#ifndef LANGUAGEPACKLISTMODEL_H
#define LANGUAGEPACKLISTMODEL_H

#include <QAbstractListModel>
#include <QIcon>
#include <QObject>
#include <QPixmap>

#include "language/LanguagePackage.h"
#include <vector>

class LanguagePackListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    enum Role { Name = Qt::UserRole + 1, Description, Language, Territory, IsNeeded, IsInstalled, Parents, FlagSource };
    Q_ENUM(Role)
    explicit LanguagePackListModel(QObject *parent = nullptr);

    const std::vector<LanguagePackage> &list() const;
    void setList(const std::vector<LanguagePackage> &newList);

    static QPixmap getCountryPixmap(const QString &territory)
    {
        auto pixmap = QIcon(":/country-flags/" + territory).pixmap(24, 24);

        return pixmap;
    }

Q_SIGNALS:
    void listChanged();

private:
    std::vector<LanguagePackage> m_list;

    // QAbstractItemModel interface
public:
    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;
};
#endif // LANGUAGEPACKLISTMODEL_H
