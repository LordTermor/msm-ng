#include "LanguagePackModule.h"

#include <KAboutData>
#include <KPluginFactory>
#include <LanguagePackListModel.h>
#include <LanguagePackViewQt.h>
#include <QIcon>

K_PLUGIN_FACTORY_WITH_JSON(KCMLanguagePackFactory, "mcp_langpack.json", registerPlugin<LanguagePackModel>();)

LanguagePackModel::LanguagePackModel(QObject *parent, const KPluginMetaData &data, const QVariantList &args)
    : KQuickAddons::ManagedConfigModule(parent, data, args)
{
    setButtons(NoAdditionalButton);

    qmlRegisterType<LanguagePackListModel>("LanguagePackView", 1, 0, "LanguagePackListModel");
    qmlRegisterType<LanguagePackViewQt>("LanguagePackView", 1, 0, "LanguagePackViewQt");
}

LanguagePackModel::~LanguagePackModel()
{
}

void LanguagePackModel::load()
{
    ManagedConfigModule::load();

    auto emptyDeleter = []([[maybe_unused]] void *view) {};

    m_presenter = new LanguagePacksPagePresenter({m_view, emptyDeleter});
}

LanguagePackViewQt *LanguagePackModel::view() const
{
    return m_view;
}

void LanguagePackModel::setView(LanguagePackViewQt *newView)
{
    if (m_view == newView)
        return;
    m_view = newView;
    Q_EMIT viewChanged();
}

#include "LanguagePackModule.moc"
