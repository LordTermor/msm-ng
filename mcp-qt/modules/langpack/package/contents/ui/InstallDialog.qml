import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.19 as Kirigami

Kirigami.Dialog {
    id: installDialog

    property alias details: detailsLabel.text
    property alias progress: installationProgressBar.value
    
    title: qsTr("Installation")

    closePolicy: Popup.NoAutoClose
    showCloseButton: false

    padding: 10

    ColumnLayout{
        ProgressBar {
            id: installationProgressBar
        }
        Label{
            text: qsTr("Installing...")
            id: detailsLabel
        }

    }
}
