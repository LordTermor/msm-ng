import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.19 as Kirigami
import org.kde.kcm 1.4 as KCM

import LanguagePackView 1.0

KCM.ScrollViewKCM {
    id: kcmRoot
    Component.onCompleted: {
        kcm.view = root
        
    }
    view: listView
    
    InstallDialog {
        id: installDialog
    }
    
    Kirigami.Dialog {
        id: confirmationDialog

        title: qsTr("Installation")
        standardButtons: Kirigami.Dialog.Yes | Kirigami.Dialog.Cancel
        padding: 10
        preferredWidth: parent.width*0.85

        ColumnLayout {
            Label {
                text: qsTr("Missing language packages are ready to install\nContinue?");
            }
        }
        onAccepted: {
            installDialog.open();
            root.installMissingPackages();
        }
    }

    LanguagePackViewQML {
        id: root
        listView: listView
        installDialog: installDialog
    }
    
    LanguagePackListView {
        header: Kirigami.InlineMessage {
            anchors {
                left: parent.left
                right: parent.right
            }
            height: 50

            anchors.margins: Kirigami.Units.smallSpacing
            showCloseButton: true
            visible: true
            text: i18n("Made by Manjaro. Report an issue.")
        }
        
        id: listView
    }
}
