import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.19 as Kirigami
import LanguagePackView 1.0

LanguagePackViewQML {
    id: root

    listView: listView
    installDialog: installDialog

    Kirigami.Dialog {
        id: confirmationDialog

        title: qsTr("Installation")
        standardButtons: Kirigami.Dialog.Yes | Kirigami.Dialog.Cancel
        padding: 10
        preferredWidth: parent.width*0.85

        ColumnLayout {
            Label {
                text: qsTr("Missing language packages are ready to install\nContinue?");
            }
        }
        onAccepted: {
            installDialog.open();
            root.installMissingPackages();
        }
    }

    InstallDialog {
        preferredWidth: parent.width*0.85

        id: installDialog
    }
    Kirigami.ScrollablePage {
        anchors.fill: parent
        
        LanguagePackListView {
            id: listView
        }
    }
}
