import QtQuick 2.15
import LanguagePackView 1.0


LanguagePackViewQt {
    id: root

    property var installDialog
    property var listView

    model: listView.model
    function updateProgress(action, status, progress /*: double */){
        if(action==="finished"){
            installDialog.title = "";
            installDialog.close();
            root.requestPackageList();
            return;
        }

        installDialog.details = action;
        installDialog.progress = progress * 100;
    }

    function installMissing(){
        confirmationDialog.open();
    }
}
