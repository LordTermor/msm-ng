import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.15 as Kirigami
import LanguagePackView 1.0

ListView {
    anchors.fill: parent

    id: listView


    model: LanguagePackListModel{
        id: listModel
    }

    delegate: LanguagePackDelegate {
        anchors {
            left: parent.left
            right: parent.right
        }

        name: model.name
        description: model.description
    }

    section {
        property: "isInstalled"
        delegate: Kirigami.ListSectionHeader {
            text: section=="true"?qsTr("Installed"):qsTr("Available")
            ToolButton{
                Layout.alignment: Qt.AlignRight
                visible: section!="true"
                icon.name: "edit-download"
                flat:true
                text: qsTr("Install")
                height: parent.height
                onClicked: root.installMissing()
            }
        }
    }
}
