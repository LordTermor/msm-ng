#include "LanguagePackViewQt.h"
#include <QQmlEngine>
LanguagePackViewQt::LanguagePackViewQt()
    : MCP::Qt::AbstractViewQML()
    , m_model(new LanguagePackListModel)
{
    setFlag(ItemHasContents);
    connect(this, &LanguagePackViewQt::installMissingPackages, [this]() {
        installMissingRequested.emit();
    });

    connect(this, &LanguagePackViewQt::requestPackageList, [this]() {
        packagesRequested.emit();
    });
}

LanguagePackListModel *LanguagePackViewQt::model() const
{
    return m_model;
}

void LanguagePackViewQt::setModel(LanguagePackListModel *newModel)
{
    if (m_model == newModel)
        return;
    m_model = newModel;
    Q_EMIT modelChanged();
}

void LanguagePackViewQt::_show()
{ /*show();*/
}

void LanguagePackViewQt::_init()
{
    packagesRequested.emit();
}

void LanguagePackViewQt::setPackageList(const std::vector<LanguagePackage> &list)
{
    if (!m_model)
        return;
    std::vector<LanguagePackage> filtered;

    std::copy_if(list.begin(), list.end(), std::back_inserter(filtered), [](const auto &el) {
        return el.needed;
    });

    m_model->setList(filtered);
}

void LanguagePackViewQt::setProgress(const std::string &action, const std::string &status, double progress)
{
    QMetaObject::invokeMethod(this,
                              "updateProgress",
                              Q_ARG(QVariant, QString::fromStdString(action)),
                              Q_ARG(QVariant, QString::fromStdString(status)),
                              Q_ARG(QVariant, progress));
}
