#include "KernelViewQt.h"
#include <QQmlEngine>
#include <QQuickItem>

KernelViewQt::KernelViewQt(QWidget *parent)
    : MCP::Qt::AbstractViewQML()
{
    connect(this, &KernelViewQt::installKernel, [this](const QString &pkgName) {
        installKernelRequested.emit(pkgName.toStdString());
    });

    connect(this, &KernelViewQt::removeKernel, [this](const QString &pkgName) {
        removeKernelRequested.emit(pkgName.toStdString());
    });

    connect(this, &KernelViewQt::requestKernelList, [this]() {
        kernelListRequested.emit();
    });

    connect(this, &KernelViewQt::requestExtraModules, [this](const QString &pkgName) {
        extraModulesRequested.emit(pkgName.toStdString());
    });
}

KernelViewQt::~KernelViewQt()
{
}

KernelListModel *KernelViewQt::model() const
{
    return m_model;
}

void KernelViewQt::setModel(KernelListModel *newModel)
{
    if (m_model == newModel)
        return;
    m_model = newModel;
    Q_EMIT modelChanged();
}

void KernelViewQt::_init()
{
    this->kernelListRequested.emit();
}

void KernelViewQt::setKernelList(const std::vector<Kernel> &list)
{
    if (!m_model)
        return;

    m_model->setList(list);
}

void KernelViewQt::setKernelProgress(const std::string &action, const std::string &status, double progress)
{
    QMetaObject::invokeMethod(this,
                              "updateProgress",
                              Q_ARG(QVariant, QString::fromStdString(action)),
                              Q_ARG(QVariant, QString::fromStdString(status)),
                              Q_ARG(QVariant, progress));
}

void KernelViewQt::setExtraModules(const std::vector<std::string> &modules)
{
    QStringList modulesQList;
    modulesQList.reserve(modules.size());

    std::transform(modules.begin(), modules.end(), std::back_inserter(modulesQList), [](const std::string &p) {
        return QString::fromStdString(p);
    });

    this->setProperty("extraModules", modulesQList);
}

void KernelViewQt::_show()
{ /* show(); */
}
