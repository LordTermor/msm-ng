#include "KernelListModel.h"
#include <QEventLoop>
#include <QMetaEnum>
#include <QNetworkReply>
#include <QNetworkRequest>

#include <kernel/KernelService.h>

KernelListModel::KernelListModel()
    : m_manager(new QNetworkAccessManager(this))
{
}

int KernelListModel::rowCount(const QModelIndex &parent) const
{
    return m_list.size();
}

QVariant KernelListModel::data(const QModelIndex &index, int role) const
{
    auto &el = m_list[index.row()];

    switch (role) {
    case Name:
        return QString::fromStdString(el.m_packageName);
    case IsInstalled:
        return el.installed;
    case Version:
        return QString::fromStdString(el.m_version);
    case IsLTS:
        return el.lts;
    case IsRecommended:
        return el.recommended;
    case IsRealTime:
        return el.real_time;
    case IsEOL:
        return el.not_supported;
    case IsExperimental:
        return el.experimental;
    case IsBooted:
        return el.booted;
    case MajorVersion:
        return el.versionPair().first;
    case MinorVersion:
        return el.versionPair().second;
    case Category:
        if (el.booted)
            return tr("In use");
        if (el.installed)
            return tr("Installed");
        return tr("Available");
    }

    return QVariant::Invalid;
}

const std::vector<Kernel> &KernelListModel::list() const
{
    return m_list;
}

void KernelListModel::setList(const std::vector<Kernel> &newList)
{
    if (m_list == newList)
        return;
    beginResetModel();
    m_list = newList;

    std::sort(m_list.begin(), m_list.end(), [](const auto &a, const auto &b) {
        return (a.versionPair().first > b.versionPair().first)
            || ((a.versionPair().first == b.versionPair().first) && (a.versionPair().second > b.versionPair().second));
    });

    std::partition(m_list.begin(), m_list.end(), [](const auto &a) {
        return a.installed;
    });
    std::partition(m_list.begin(), m_list.end(), [](const auto &a) {
        return a.booted;
    });

    endResetModel();

    Q_EMIT listChanged();
}

void KernelListModel::loadChangelog(int major, int minor, const QJSValue &cb) const
{
    static constexpr char url[] = "https://kernelnewbies.org/";

    if (!cb.isCallable()) {
        return;
    }

    auto changelog = KernelService::getChangelog({major, minor});

    if (changelog.length() != 0) {
        QJSValue cbCopy(cb);
        cbCopy.call({QString::fromStdString(changelog)});
        return;
    }

    QNetworkRequest rq;
    rq.setUrl(QLatin1String(url) + "/Linux_" + QString::number(major) + "." + QString::number(minor) + "?action=print");
    rq.setRawHeader("Accept-Language", "en_US");

    auto reply = m_manager->get(rq);

    connect(reply, &QNetworkReply::finished, this, [reply, cb]() {
        QString cl = reply->readAll();
        QJSValue cbCopy(cb);

        if (cl.contains("This page does not exist yet")) {
            cl = tr("<b>No changelogs found</b>");
        }

        cbCopy.call({cl});

        delete reply;
    });
}

QHash<int, QByteArray> KernelListModel::roleNames() const
{
    QHash<int, QByteArray> result;
    auto enumeration = QMetaEnum::fromType<Role>();
    for (int i = 0; i < enumeration.keyCount(); i++) {
        result[i + Qt::UserRole + 1] = enumeration.key(i);
        result[i + Qt::UserRole + 1][0] = std::tolower(result[i + Qt::UserRole + 1][0]);
    }
    return result;
}
