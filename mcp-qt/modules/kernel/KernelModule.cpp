#include "KernelModule.h"

#include <KAboutData>
#include <KPluginFactory>
#include <KernelListModel.h>
#include <KernelViewQt.h>
#include <QIcon>

K_PLUGIN_FACTORY_WITH_JSON(KCMKernelFactory, "mcp_kernel.json", registerPlugin<KernelModule>();)

KernelModule::KernelModule(QObject *parent, const KPluginMetaData &data, const QVariantList &args)
    : KQuickAddons::ManagedConfigModule(parent, data, args)
{
    setButtons(NoAdditionalButton);

    qmlRegisterType<KernelListModel>("KernelView", 1, 0, "KernelListModel");
    qmlRegisterType<KernelViewQt>("KernelView", 1, 0, "KernelViewQt");
}

KernelModule::~KernelModule()
{
}

void KernelModule::load()
{
    ManagedConfigModule::load();

    auto emptyDeleter = []([[maybe_unused]] void *view) {};

    m_presenter = new KernelsPagePresenter({m_view, emptyDeleter});
}

KernelViewQt *KernelModule::view() const
{
    return m_view;
}

void KernelModule::setView(KernelViewQt *newView)
{
    if (m_view == newView)
        return;
    m_view = newView;
    Q_EMIT viewChanged();
}

#include "KernelModule.moc"
