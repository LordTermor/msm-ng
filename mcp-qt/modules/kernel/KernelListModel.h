#pragma once
#include "kernel/Kernel.h"
#include <QAbstractListModel>
#include <QJSValue>
#include <QNetworkAccessManager>

class KernelListModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(std::vector<Kernel> list READ list WRITE setList NOTIFY listChanged)
public:
    enum Role {
        Name = Qt::UserRole + 1,
        IsInstalled,
        Version,
        IsLTS,
        IsRecommended,
        IsRealTime,
        IsEOL,
        IsExperimental,
        IsBooted,
        MajorVersion,
        MinorVersion,
        Category,
        Changelog
    };
    Q_ENUM(Role)
    KernelListModel();

    const std::vector<Kernel> &list() const;
    void setList(const std::vector<Kernel> &newList);

Q_SIGNALS:
    void listChanged();

public:
    Q_INVOKABLE void loadChangelog(int major, int minor, const QJSValue &cb) const;

private:
    std::vector<Kernel> m_list;
    QNetworkAccessManager *m_manager;

    // QAbstractItemModel interface
public:
    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;
};
