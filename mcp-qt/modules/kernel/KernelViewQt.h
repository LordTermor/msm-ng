#ifndef KERNELPAGEQT_H
#define KERNELPAGEQT_H

#include "KernelListModel.h"
#include "modules/kernel/KernelAbstractView.h"
#include <AbstractViewQt.h>

class KernelViewQt : public MCP::Qt::AbstractViewQML, public KernelAbstractView
{
    Q_OBJECT

public:
    explicit KernelViewQt(QWidget *parent = nullptr);
    ~KernelViewQt();

private:
    KernelListModel *m_model;

    Q_PROPERTY(KernelListModel *model READ model WRITE setModel NOTIFY modelChanged)

Q_SIGNALS:
    void installKernel(const QString &pkgName);
    void removeKernel(const QString &pkgName);
    void requestKernelList();
    void requestExtraModules(const QString &pkgName);

    // KernelAbstractView interface
    void modelChanged();

public:
    void _init() override;
    void setKernelList(const std::vector<Kernel> &list) override;
    void setKernelProgress(const std::string &action, const std::string &status, double progress) override;
    void setExtraModules(const std::vector<std::string> &modules) override;

    // AbstractView interface
public:
    virtual void _show() override;
    KernelListModel *model() const;
    void setModel(KernelListModel *newModel);
};

#endif // KERNELPAGEQT_H
