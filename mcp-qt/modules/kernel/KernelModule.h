/**
 * SPDX-FileCopyrightText: 2022 Artem Grinev <agrinev@manjaro.org>
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <KQuickAddons/ManagedConfigModule>
#include <KernelViewQt.h>
#include <QPixmap>
#include <modules/kernel/KernelsPagePresenter.h>

class KernelModule : public KQuickAddons::ManagedConfigModule
{
    Q_OBJECT
    Q_PROPERTY(KernelViewQt *view READ view WRITE setView NOTIFY viewChanged)

public:
    KernelModule(QObject *parent, const KPluginMetaData &data, const QVariantList &args);
    virtual ~KernelModule() override;
    // ConfigModule interface
    KernelViewQt *view() const;
    void setView(KernelViewQt *newView);

public Q_SLOTS:
    virtual void load() override;

Q_SIGNALS:
    void viewChanged();

private:
    KernelsPagePresenter *m_presenter;
    KernelViewQt *m_view;
};
