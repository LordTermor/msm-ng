import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.15 as Kirigami

Kirigami.OverlaySheet{
    id: changelogSheet

    property var majorVersion
    property var minorVersion
    property string text: ""

    onTextChanged: {
        console.log(loadingProgressBar.visible);
    }


    header: Kirigami.Heading {
        textFormat: Text.RichText
        text: qsTr(`${changelogSheet.majorVersion}.${changelogSheet.minorVersion} Changelog ― by <a href=\'https://kernelnewbies.org\'>Linux Kernel Newbies</a>`)
        onLinkActivated: Qt.openUrlExternally(link)
    }

    ProgressBar {
        id: loadingProgressBar
        visible: changelogSheet.text.length === 0
        indeterminate: true
        height: 50
        width: parent.width

        onVisibleChanged: {
            console.log(visible)
        }
    }

    TextArea {
        background: Item{}
        font.pointSize: 9
        implicitWidth: root.width * 0.8
        rightPadding: 20
        readOnly: true
        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
        textFormat: Text.RichText
        id: changelogText
        text: changelogSheet.text
        onLinkActivated: {
            if(link.toString().startsWith("#")){
                return
            }

            Qt.openUrlExternally(link)
        }
    }
}
