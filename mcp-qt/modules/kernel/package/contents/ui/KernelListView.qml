import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.15 as Kirigami
import KernelView 1.0

ListView {
    id: listView
    property var changelogSheet
    model: KernelListModel{
        id: listModel
    }

    delegate: KernelDelegate {
        anchors.horizontalCenter: parent.horizontalCenter
        width: parent.width-2
        onShowChangelog: {
            changelogSheet.majorVersion = majorVersion;
            changelogSheet.minorVersion = minorVersion;
            changelogSheet.text = "";
            changelogSheet.open();
            listModel.loadChangelog(majorVersion, minorVersion, (str)=>{
                                        if(changelogSheet.majorVersion === majorVersion &&
                                           changelogSheet.minorVersion === minorVersion) {
                                            changelogSheet.text = str;
                                        }});
        }

    }
    section {
        property: "category"
        delegate: Kirigami.ListSectionHeader {
            text: section
        }
    }
}
