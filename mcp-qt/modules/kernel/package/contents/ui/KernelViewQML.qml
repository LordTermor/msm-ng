import QtQuick 2.15
import KernelView 1.0

KernelViewQt {
    id: root
    model: listView.model

    property var listView
    property var installDialog
    property var confirmationDialog

    property string currentlyInstallingKernel
    property var extraModules: []


    signal kernelListRequested


    function installKernelDialog(pkgname){
        confirmationDialog.isRemoveDialog = false;
        currentlyInstallingKernel = pkgname;
        confirmationDialog.open();

        root.requestExtraModules(pkgname);
    }

    function removeKernelDialog(pkgname){
        confirmationDialog.isRemoveDialog = true;
        currentlyInstallingKernel = pkgname;
        confirmationDialog.open();
    }

    function updateProgress(action, status, progress /*: double */){
        if(action==="finished"){
            installDialog.details = "";
            root.currentlyInstallingKernel = "";
            root.requestKernelList();
            installDialog.close()
            return;
        }

        installDialog.details = action;
        installDialog.progress = progress * 100;

    }

}
