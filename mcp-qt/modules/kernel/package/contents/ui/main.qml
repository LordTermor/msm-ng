import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Layouts 1.15
import org.kde.kirigami 2.19 as Kirigami
import org.kde.kcm 1.4 as KCM

KCM.ScrollViewKCM {
    id: kcmRoot
    Component.onCompleted: {
        kcm.view = root
    }
    view: listView

    InstallDialog {
        id: installDialog

        preferredWidth: parent.width*0.85

        details: confirmationDialog.isRemoveDialog
                 ? qsTr("Uninstalling...")
                 : qsTr("Installing")
    }

    Kirigami.Dialog {

        id: confirmationDialog

        property bool isRemoveDialog: false

        title: isRemoveDialog?qsTr("Uninstallation"):qsTr("Installation")
        standardButtons: Kirigami.Dialog.Yes | Kirigami.Dialog.Cancel
        padding: 10
        preferredWidth: parent.width*0.85

        ColumnLayout {
            Label {
                textFormat: Text.MarkdownText
                text: confirmationDialog.isRemoveDialog
                      ? qsTr("Do you want to remove `%1`?").arg(currentlyInstallingKernel)
                      : qsTr("New kernel packages are ready to install<br />Would you like to continue?")
            }

            ListView {
                visible: (extraModules.length > 0) && !confirmationDialog.isRemoveDialog
                Layout.preferredHeight: contentItem.height
                model: root.extraModules
                header: Label {
                    text: qsTr("Following extra packages will be installed:")
                }

                delegate: Label {
                    textFormat: Text.MarkdownText
                    width: parent.width
                    height: paintedHeight
                    text: "* "+modelData
                }
            }
        }
        onAccepted: {
            installDialog.open();
            if(confirmationDialog.isRemoveDialog) {
                root.removeKernel(root.currentlyInstallingKernel);
            } else {
                root.installKernel(root.currentlyInstallingKernel);
            }
        }
        onRejected: {
            root.currentlyInstallingKernel = "";
        }
    }

    ChangelogSheet {
        rightInset: 35
        leftInset: 35
        id: changelogSheet
    }

    KernelListView {
        changelogSheet: changelogSheet
        id: listView
    }

    KernelViewQML {
        listView: listView
        installDialog: installDialog
        confirmationDialog: confirmationDialog
        id: root
    }
}
