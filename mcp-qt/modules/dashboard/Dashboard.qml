import QtQuick 2.15
import QtQuick.Controls 2.15 as Controls
import org.kde.kirigami 2.15 as Kirigami
import QtQuick.Layouts 1.15

Kirigami.Page {
    Controls.Button{
        anchors{
            bottom: parent.bottom
            right: parent.right
        }

        text: qsTr("Copy a detailed system information")
    }
    anchors.fill: parent

    ColumnLayout {
        anchors.margins: 20
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.horizontalCenter: parent.horizontalCenter
        Image {
            source: "qrc:/hello-icons/manjaro.png"
        }
        Controls.Label{
            text: qsTr("System: Manjaro Linux 21.1 Pahvo")
        }
        Controls.Label{
            text: qsTr("Branch: unstable")
        }
        Controls.Label{
            text: qsTr("Uptime: 2h 35m 2s")
        }
        Controls.Label{
            text: qsTr("Sound server: PipeWire")
        }
        Controls.Label{
            text: qsTr("Display server: Wayland")
        }


        Item{
            Layout.preferredHeight: 10
        }

        ListView{
            Layout.fillHeight: true
            Layout.fillWidth: true
            header: Kirigami.ListSectionHeader{
                text: "Potential system problems:"
            }

            model: ListModel{
                ListElement{
                    title: "Kernel is EOL!"
                    urgency: "Urgent"
                }
            }
            delegate: Kirigami.BasicListItem {
                Kirigami.Theme.colorSet: Kirigami.Theme.View
                Kirigami.Theme.inherit: false
                backgroundColor: Kirigami.Theme.backgroundColor
                highlighted: false
                clip: true
                onClicked: description.Layout.preferredHeight===0?
                     revealAnimation.start()
                             :hideAnimation.start()


                ParallelAnimation{
                    running: false
                    id: revealAnimation

                    NumberAnimation{
                        target: description
                        property: "opacity"
                        easing.type: Easing.InOutQuad
                        from: 0.0
                        to: 1.0
                        duration: 100
                    }
                    NumberAnimation{
                        target: description
                        property: "Layout.preferredHeight"
                        easing.type: Easing.InOutQuad
                        from: 0.0
                        to: description.paintedHeight
                        duration: 100
                    }

                }
                ParallelAnimation{
                    running: false
                    id: hideAnimation

                    NumberAnimation{
                        target: description
                        property: "opacity"
                        easing.type: Easing.InOutQuad
                        to: 0.0
                        from: 1.0
                        duration: 100
                    }
                    NumberAnimation{
                        target: description
                        property: "Layout.preferredHeight"
                        easing.type: Easing.InOutQuad
                        to: 0.0
                        from: description.paintedHeight
                        duration: 100
                    }

                }


                contentItem: GridLayout{
                    anchors.margins: 5

                    rows: 2
                    columns: 3
                    Kirigami.Icon{

                        Layout.column: 0
                        Layout.row: 0

                        source: "data-warning"
                    }
                    Controls.Label{

                        Layout.column: 1
                        Layout.row: 0

                        text: model.title
                    }
                    Kirigami.Icon{

                        Layout.column: 2
                        Layout.row: 0

                        source: "documentinfo"
                    }
                    Controls.Label{
                        id: description
                        opacity: 0
                        Layout.preferredHeight: 0
                        Layout.preferredWidth: parent.width
                        Layout.column: 0
                        Layout.columnSpan: 3
                        Layout.row: 1

                        wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                        text: "EOL (End of Life) means that this kernel version will no longer receive maintenance update.\nNavigate to Kernels module and install another version"
                    }

                }
            }

        }
    }
}
