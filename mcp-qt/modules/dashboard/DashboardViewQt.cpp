#include "DashboardViewQt.h"

namespace MCP::Qt
{

DashboardViewQt::DashboardViewQt(QWidget *parent)
    : QQuickWidget(parent)
{
    setSource(QUrl("qrc:/modules/dashboard/Dashboard.qml"));
    setResizeMode(QQuickWidget::SizeRootObjectToView);
}

void DashboardViewQt::_show()
{
    show();
}

void DashboardViewQt::_init()
{
}

void DashboardViewQt::setSystemIssues()
{
}

} // namespace MCP::Qt
