#pragma once

#include <QQuickWidget>
#include <modules/dashboard/DashboardView.h>

namespace MCP::Qt
{

class DashboardViewQt : public QQuickWidget, public DashboardView
{
    Q_OBJECT
public:
    explicit DashboardViewQt(QWidget *parent = nullptr);

Q_SIGNALS:

    // AbstractView interface
public:
    void _show() override;
    void _init() override;

    // DashboardView interface
public:
    void setSystemIssues() override;
};

} // namespace MCP::Qt
