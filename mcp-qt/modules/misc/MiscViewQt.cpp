#include "MiscViewQt.h"
#include "ui_MiscViewQt.h"
#include <QDialogButtonBox>
MiscViewQt::MiscViewQt(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::MiscViewQt)
{
    ui->setupUi(this);

    connect(ui->enabledSpeakerCheck, &QCheckBox::clicked, [this](bool checked) {
        changeSpeakerEnabledRequested.emit(checked);
    });
}

MiscViewQt::~MiscViewQt()
{
    delete ui;
}

void MiscViewQt::_show()
{
    show();
}

void MiscViewQt::_init()
{
    settingsValuesRequested.emit();
}

void MiscViewQt::setSettingsValues(const std::map<std::string, std::any> &values)
{
    bool spkr = std::any_cast<bool>(values.at("spkr"));
    bool spkrEditable = std::any_cast<bool>(values.at("spkrEditable"));
    ui->enabledSpeakerCheck->setChecked(spkr);

    if (!spkr) {
        ui->enabledSpeakerCheck->setEnabled(spkrEditable);
    }

    bool hwInLocal = std::any_cast<bool>(values.at("hwLocal"));
    ui->hWClockInLocalCheck->setChecked(hwInLocal);

    bool timeZoneSync = std::any_cast<bool>(values.at("tzSync"));
    ui->timeZoneSyncCheck->setChecked(timeZoneSync);
}

void MiscViewQt::on_buttonBox_clicked(QAbstractButton *button)
{
    switch (ui->buttonBox->buttonRole(button)) {
    case QDialogButtonBox::ApplyRole:
        saveRequested.emit();
        break;
    case QDialogButtonBox::ResetRole:

        break;
    default:
        break;
    }
}
