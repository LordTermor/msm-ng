#ifndef MISCVIEWQT_H
#define MISCVIEWQT_H

#include <QAbstractButton>
#include <QWidget>
#include <modules/misc/MiscAbstractView.h>
namespace Ui
{
class MiscViewQt;
}

class MiscViewQt : public QWidget, public MiscAbstractView
{
    Q_OBJECT

public:
    explicit MiscViewQt(QWidget *parent = nullptr);
    ~MiscViewQt();

private:
    Ui::MiscViewQt *ui;

private Q_SLOTS:
    void on_buttonBox_clicked(QAbstractButton *button);

    // AbstractView interface
public:
    virtual void _show() override;
    virtual void _init() override;

    // MiscAbstractView interface
    virtual void setSettingsValues(const std::map<std::string, std::any> &values) override;
};

#endif // MISCVIEWQT_H
