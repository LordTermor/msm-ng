#pragma once

#include <AbstractView.h>
#include <QQmlComponent>
#include <QQuickItem>

namespace MCP::Qt
{
class AbstractViewQML : public QQuickItem, public virtual AbstractView
{
public:
    QQmlComponent *component() const;
    void setComponent(QQmlComponent *newComponent);

private:
    QQmlComponent *m_component;
};

inline QQmlComponent *AbstractViewQML::component() const
{
    return m_component;
}

inline void AbstractViewQML::setComponent(QQmlComponent *newComponent)
{
    m_component = newComponent;
}

} // namespace MCP::Qt
