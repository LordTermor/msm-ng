class QObject;

namespace Kirigami
{
class PlatformTheme;
class KirigamiPluginFactory
{
public:
    static KirigamiPluginFactory *findPlugin();
    Kirigami::PlatformTheme *createPlatformTheme(QObject *parent);
};

}

Kirigami::KirigamiPluginFactory *Kirigami::KirigamiPluginFactory::findPlugin()
{
    return nullptr;
}

Kirigami::PlatformTheme *Kirigami::KirigamiPluginFactory::createPlatformTheme(QObject *parent)
{
    return nullptr;
}
