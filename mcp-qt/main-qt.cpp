#include <QApplication>

#include "MainPresenter.h"
#include "modules/dashboard/DashboardPresenter.h"
#include "modules/kernel/KernelsPagePresenter.h"
#include "modules/lang/LanguagePacksPagePresenter.h"
#include "modules/misc/MiscPresenter.h"
#include <LegacySupport.h>

#include "MainViewQt.h"
#include "modules/dashboard/DashboardViewQt.h"
#include "modules/kernel/KernelListModel.h"
#include "modules/kernel/KernelViewQt.h"
#include "modules/langpack/LanguagePackListModel.h"
#include "modules/langpack/LanguagePackViewQt.h"
#include "modules/misc/MiscViewQt.h"
#include <modules/mhwd-legacy/MhwdPage.h>

#include <QQmlDebuggingEnabler>
#include <QQmlEngine>
#include <QtQuickControls2/QQuickStyle>

QQmlDebuggingEnabler enabler;

// Qt will handle deletion
auto emptyDeleter = []([[maybe_unused]] void *view) {};

template<typename TPresenter, typename TView>
TPresenter *createQmlPresenter(const QUrl &url, QQmlEngine *engine)
{
    if (!engine)
        return nullptr;

    auto component = new QQmlComponent(engine, url);

    if (component->isError()) {
        qDebug() << "Error creating a new QML presenter, component returned errors:";
        auto errors = component->errors();
        for (const auto &error : std::as_const(errors)) {
            qDebug() << error;
        }
        return nullptr;
    }

    auto *object = component->create();
    if (!object) {
        qDebug() << "Error creating a new QML presenter, component couldn't create an object";
        return nullptr;
    }
    auto *item = dynamic_cast<TView *>(object);
    item->setComponent(component);

    if (!object) {
        qDebug() << "Error creating a new QML presenter, component and provided view type do not match";
        return nullptr;
    }

    return new TPresenter({item, emptyDeleter});
}

int main(int argc, char **argv)
{
    QApplication app(argc, argv);

    if (qgetenv("XDG_CURRENT_DESKTOP") != "KDE") {
        QQuickStyle::setFallbackStyle("Fusion");
    }

    qmlRegisterType<KernelListModel>("KernelView", 1, 0, "KernelListModel");
    qmlRegisterType<KernelViewQt>("KernelView", 1, 0, "KernelViewQt");
    qmlRegisterType<LanguagePackListModel>("LanguagePackView", 1, 0, "LanguagePackListModel");
    qmlRegisterType<LanguagePackViewQt>("LanguagePackView", 1, 0, "LanguagePackViewQt");

    auto *engine = new QQmlEngine;

    auto *kernelPresenter = createQmlPresenter<KernelsPagePresenter, KernelViewQt>(QUrl("qrc:/modules/kernel/package/contents/ui/KernelList.qml"), engine);

    auto *languagePackPresenter =
        createQmlPresenter<LanguagePacksPagePresenter, LanguagePackViewQt>(QUrl("qrc:/modules/langpack/package/contents/ui/LanguagePackList.qml"), engine);

    auto mhwdPresenter = new MCP::LegacyPresenter<MhwdPage>({new MhwdPage, emptyDeleter});

    auto mainview = new MainViewQt;
    MainPresenter mainPresenter(mainview);

    mainPresenter.addPresenter(kernelPresenter,
                               {QCoreApplication::translate("KernelViewQt", "Kernels").toStdString(),
                                QCoreApplication::translate("KernelViewQt", "Install or remove kernels").toStdString(),
                                "yast-kernel"});
    mainPresenter.addPresenter(languagePackPresenter,
                               {QCoreApplication::translate("LanguagePackViewQt", "Language Packs").toStdString(),
                                QCoreApplication::translate("LanguagePackViewQt", "Manage language packages").toStdString(),
                                "yast-language"});
    mainPresenter.addPresenter(mhwdPresenter,
                               {QCoreApplication::translate("MhwdPage", "Hardware").toStdString(),
                                QCoreApplication::translate("MhwdPage", "Drivers support").toStdString(),
                                "yast-hardware-group"});

    mainview->_show();

    return QApplication::exec();
}
