#ifndef KERNELVIEWGTK_H
#define KERNELVIEWGTK_H
#include "modules/kernel/KernelAbstractView.h"

#include <gtkmm/builder.h>
#include <gtkmm/listbox.h>
#include <gtkmm/window.h>
class KernelViewGTK : public Gtk::Widget, public KernelAbstractView {
public:
    KernelViewGTK();
    KernelViewGTK(BaseObjectType *cobject,
                  const Glib::RefPtr<Gtk::Builder> &refBuilder);

private:
    Gtk::ListBox *installedBox;
    Gtk::ListBox *availableBox;

    // KernelAbstractView interface
public:
    void _init() override;
    void setKernelList(const std::vector<Kernel> &list) override;
    void setKernelProgress(const std::string &action,
                           const std::string &status,
                           double progress) override;

    // AbstractView interface
public:
    virtual void _show() override;
};

#endif // KERNELVIEWGTK_H
