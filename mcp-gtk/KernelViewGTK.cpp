#include "KernelViewGTK.h"

#include <gdkmm/color.h>
#include <gtkmm/button.h>
#include <gtkmm/cssprovider.h>
#include <gtkmm/eventbox.h>
#include <gtkmm/image.h>
#include <gtkmm/label.h>
#include <gtkmm/separator.h>
#include <iostream>

Gdk::RGBA blend(const Gdk::RGBA &fg, const Gdk::RGBA &bg) {
    Gdk::RGBA color;
    unsigned int alpha = fg.get_alpha_u() + 1;
    unsigned int inv_alpha = 256 - fg.get_alpha_u();
    color.set_red_u(
        (unsigned char)((alpha * fg.get_red_u() + inv_alpha * bg.get_red_u())
                        >> 8));
    color.set_green_u((
        unsigned char)((alpha * fg.get_green_u() + inv_alpha * bg.get_green_u())
                       >> 8));
    color.set_blue_u(
        (unsigned char)((alpha * fg.get_blue_u() + inv_alpha * bg.get_blue_u())
                        >> 8));
    color.set_alpha_u(0xFF);

    return color;
}

KernelViewGTK::KernelViewGTK() {
}

KernelViewGTK::KernelViewGTK(Gtk::Widget::BaseObjectType *cobject,
                             const Glib::RefPtr<Gtk::Builder> &refBuilder)
    : Gtk::Widget(cobject) {
    refBuilder->get_widget("box", installedBox);
    refBuilder->get_widget("availableBox", availableBox);

    //    set_title("mcp-ng-gtk");
}

void KernelViewGTK::_init() {
    kernelListRequested.emit();
}

void KernelViewGTK::setKernelList(const std::vector<Kernel> &list) {
    std::vector<Kernel> lstCopy(list.size());
    std::copy(list.begin(), list.end(), lstCopy.begin());
    std::sort(lstCopy.begin(), lstCopy.end(), [](auto &kernelA, auto &kernelB) {
        return kernelA.m_version > kernelB.m_version;
    });

    for (auto &el : lstCopy) {
        auto builder = Gtk::Builder::create_from_file("KernelViewItem.glade");

        Gtk::EventBox *wgt;
        builder->get_widget("root", wgt);
        Gtk::Label *versionLabel;

        builder->get_widget("versionLabel", versionLabel);

        versionLabel->set_text(el.m_version.substr(0, 3));

        Gtk::Label *label;

        builder->get_widget("nameLabel", label);

        label->set_text("Linux " + el.m_version.substr(0, 3));

        //        Gtk::Label* packageNameLabel;

        //        builder->get_widget("packageNameLabel",packageNameLabel);

        //        packageNameLabel->set_text(el.m_packageName);

        Gtk::Button *installButton;

        builder->get_widget("installButton", installButton);

        if (el.booted) {
            installButton->hide();

            Gtk::Image *isBootedImage;

            builder->get_widget("isBootedImage", isBootedImage);

            isBootedImage->show();

        } else {
            installButton->set_image_from_icon_name(
                el.installed ? "edit-delete-symbolic"
                             : "folder-download-symbolic");

            installButton->set_size_request(40);

            installButton->signal_clicked().connect([=, this]() {
                this->installKernelRequested.emit(el.m_packageName);
            });
        }

        Gtk::Image *isRecommendedImage;
        Gtk::Label *isRTLabel, *isLtsLabel;

        builder->get_widget("isRTLabel", isRTLabel);
        builder->get_widget("isLtsLabel", isLtsLabel);
        builder->get_widget("isRecommendedImage", isRecommendedImage);

        isRTLabel->set_visible(el.real_time);
        isRecommendedImage->set_visible(el.recommended);

        isLtsLabel->set_visible(el.lts);

        Gdk::RGBA clr;
        if (el.recommended) {
            clr.set("#76fc90");
        } else if (el.lts) {
            clr.set("#dbf3fa");
        } else if (el.experimental) {
            clr.set("#fff9c4");
        } else if (el.not_supported) {
            clr.set("#f4ff81");
        } else {
            clr.set_alpha_u(0x00);
        }
        wgt->set_name("content_" + el.m_packageName);

        Glib::ustring data = "#content_" + el.m_packageName
                             + " {background-color: " + clr.to_string() + ";}";
        auto css = Gtk::CssProvider::create();
        css->load_from_data(data);
        auto screen = Gdk::Screen::get_default();
        wgt->get_style_context()->add_provider_for_screen(
            screen, css, GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

        if (el.installed) {
            installedBox->add(*wgt);
        } else {
            availableBox->add(*wgt);
        }
    }
}

void KernelViewGTK::setKernelProgress(const std::string &action,
                                      const std::string &status,
                                      double progress) {
}

void KernelViewGTK::_show() {
    show();
}
