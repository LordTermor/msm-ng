#ifndef LDMSERVICE_H
#define LDMSERVICE_H
#include "Device.h"

#include <vector>

class LDMService {
public:
    LDMService();

    std::vector<Device> getDevices();
};

#endif // LDMSERVICE_H
