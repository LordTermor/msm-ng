#ifndef DEVICE_H
#define DEVICE_H
#include "common/Utils.h"

#include <string>
#include <vector>
class Device {
public:
    Device() {}
    std::string name() const {}
    std::string vendor() const {}
    std::string modalias() const {}
    std::vector<Device> children() const {}
};

#endif // DEVICE_H
