#include "LDMService.h"

#include <common/Utils.h>

static auto manager = ldm_manager_new(LDM_MANAGER_FLAGS_NONE);

LDMService::LDMService() {
}

std::vector<Device> LDMService::getDevices() {
    return Utils::gPtrArrayToVector<Device>(
        ldm_manager_get_devices(manager, LDM_DEVICE_TYPE_ANY),
        [](void *dev) { return Device(reinterpret_cast<LdmDevice *>(dev)); });
}
