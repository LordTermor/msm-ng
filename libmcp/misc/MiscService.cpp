#include "MiscService.h"

#include <common/Utils.h>
#include <filesystem>
#include <string>
template<typename T>
bool appendIfSet(std::string &string,
                 const std::string &key,
                 const std::optional<T> &value) {
    if (value.has_value()) {
        string += key;
        string += "=";
        string += std::to_string(value.value());
        string += " ";
        return true;
    }
    return false;
}

MiscService::MiscService() {
}

void MiscService::setPCSpeakerEnabled(bool enabled) {
    enableSpeaker = enabled;
}

void MiscService::setTimeZoneSync(bool enabled) {
    enableTimeZoneSync = enabled;
}

void MiscService::setHWClockInLocal(bool enabled) {
    enableHWClockInLocal = enabled;
}

bool MiscService::getPCSpeakerEnabled(bool *editable) {
    auto output = Utils::exec("lsmod | grep pcspkr");

    bool result = output.starts_with("pcspkr");

    if (!result) {
        *editable = std::filesystem::exists("/etc/modprobe.d/mcp-nobeep.conf");
    }

    return result;
}

bool MiscService::getTimeZoneSync() {
    return false;
}

bool MiscService::getHWClockInLocal() {
    return false;
}
bool MiscService::save() {
    return runHelper();
}

bool MiscService::runHelper() {
    std::string cmdLineArgs;

    appendIfSet(cmdLineArgs, "spkr", enableSpeaker);
    appendIfSet(cmdLineArgs, "tzSync", enableTimeZoneSync);
    appendIfSet(cmdLineArgs, "hwLocal", enableHWClockInLocal);

    auto globalPath =
        std::string(INSTALL_PREFIX) + "/lib/mcp-ng/libmcp-authhelper";
    auto localPath =
        std::filesystem::absolute(std::filesystem::current_path().string()
                                  + "/../libmcp/authhelper/libmcp-authhelper")
            .string();

    if (std::filesystem::exists(globalPath)) {
        return system(
            ("/usr/bin/pkexec " + globalPath + " " + cmdLineArgs).data());
    } else if (std::filesystem::exists(localPath)) {
        return system(
            ("/usr/bin/pkexec " + localPath + " " + cmdLineArgs).data());
    }
    return false;
}
