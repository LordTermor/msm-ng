#ifndef MISCSERVICE_H
#define MISCSERVICE_H
#include <map>
#include <optional>

class MiscService {
public:
    MiscService();

    void setPCSpeakerEnabled(bool enabled);
    void setTimeZoneSync(bool enabled);
    void setHWClockInLocal(bool enabled);

    bool getPCSpeakerEnabled(bool *editable = nullptr);
    bool getTimeZoneSync();
    bool getHWClockInLocal();

    bool save();

private:
    std::optional<bool> enableSpeaker;
    std::optional<bool> enableTimeZoneSync;
    std::optional<bool> enableHWClockInLocal;

    bool runHelper();
};

#endif // MISCSERVICE_H
