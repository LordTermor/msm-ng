#pragma once
#include <filesystem>
#include <string>

struct LayoutItem {
    std::string name;
    std::filesystem::path scriptPath;
};
