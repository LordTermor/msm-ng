#ifndef KERNEL_H
#define KERNEL_H
#include <common/Utils.h>
#include <cstring>
#include <optional>
#include <pamac.h>
#include <string>
#include <sys/utsname.h>

struct Kernel {
    explicit Kernel(PamacAlpmPackage *package);

    static std::optional<Kernel>
        getFromDatabase(const std::string &kernelName,
                        const Database &db = Utils::db);

    static std::pair<int, int> parseVersion(const std::string &versionstr);

    static std::string getCurrentKernelName();

    std::pair<int, int> versionPair() const { return parseVersion(m_version); }

    Kernel() = default;

    std::string m_packageName;
    std::string m_version;

    bool lts : 1;
    bool recommended : 1;
    bool installed : 1;
    bool not_supported : 1;
    bool real_time : 1;
    bool booted : 1;
    bool experimental : 1;
};
inline bool operator==(const Kernel &lhs, const Kernel &rhs) {
    return lhs.m_packageName == rhs.m_packageName
           && lhs.m_version == rhs.m_version;
}
#endif // KERNEL_H
