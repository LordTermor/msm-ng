#include "KernelService.h"

#include "common/Utils.h"
#include "pamac.h"

#include <fstream>
#include <iostream>
#include <sstream>
using namespace Utils;
KernelService::KernelService() {
    transaction.emit_action_progress.connect([this](const std::string &action,
                                                    const std::string &status,
                                                    double progress) {
        actionProgressEmitted.emit(action, status, progress);
    });

    transaction.emit_hook_progress.connect(
        [this](const std::string &action,
               [[maybe_unused]] const std::string &details,
               const std::string &status, double progress) {
            actionProgressEmitted.emit(action, status, progress);
        });
    transaction.finished.connect([this]([[maybe_unused]] bool success) {
        actionProgressEmitted.emit("finished", "", 1);
    });

    pamac_config_set_simple_install(pamac_database_get_config(db.handle()),
                                    true);
}

KernelService::~KernelService() {
}

std::vector<Kernel> KernelService::getKernels() {
    auto list = pamac_database_search_repos_pkgs(
        db.handle(), "^linux([0-9][0-9]?([0-9])|[0-9][0-9]?([0-9])-rt)$");

    std::vector<Kernel> result;

    g_ptr_array_foreach(
        list,
        [](void *value, void *resultPtr) {
            auto result = reinterpret_cast<std::vector<Kernel> *>(resultPtr);
            Kernel krn(reinterpret_cast<PamacAlpmPackage *>(value));
            result->emplace_back(krn);
        },
        &result);

    g_ptr_array_free(list, true);
    return result;
}

bool KernelService::installKernel(const std::string &packageName) {
    pamac_transaction_add_pkg_to_install(transaction.handle(),
                                         packageName.c_str());

    transaction.run();

    return true;
}

bool KernelService::removeKernel(const std::string &packageName) {
    pamac_transaction_add_pkg_to_remove(transaction.handle(),
                                        packageName.c_str());
    pamac_transaction_add_pkg_to_remove(transaction.handle(),
                                        (packageName + "-headers").c_str());

    for (const auto &module : getExtraModules(packageName)) {
        pamac_transaction_add_pkg_to_remove(transaction.handle(),
                                            module.c_str());
    }

    transaction.run();

    return true;
}

std::string KernelService::getChangelog(const std::pair<int, int> &pkgName) {
    try {
        std::string name = std::string(INSTALL_PREFIX)
                           + "/share/mcp/changelogs/"
                           + std::to_string(pkgName.first) + "."
                           + std::to_string(pkgName.second) + ".html";

        std::ifstream stream(name);

        std::stringstream buffer;
        buffer << stream.rdbuf();

        return buffer.str();
    } catch (...) { return ""; }
}

std::vector<std::string>
    KernelService::getExtraModules(const std::string &pkgName) {
    std::vector<std::string> result;

    auto currentPkgName = Kernel::getCurrentKernelName();

    auto list = pamac_database_get_group_pkgs(
        db.handle(), (currentPkgName + "-extramodules").c_str());

    for (uint i = 0; i < list->len; i++) {
        auto modulePackage = reinterpret_cast<PamacPackage *>(list->pdata[i]);

        if (!pamac_package_get_installed_version(modulePackage)) { continue; }

        std::string moduleName = pamac_package_get_name(modulePackage);

        auto dashPosition = moduleName.find("-");

        auto targetModuleName = moduleName.substr(dashPosition);

        if (targetModuleName.empty()) { continue; }

        targetModuleName = pkgName + targetModuleName;

        auto pkg =
            pamac_database_get_pkg(db.handle(), targetModuleName.c_str());

        if (!pkg) { continue; }

        result.emplace_back(targetModuleName);
    }
    g_ptr_array_free(list, true);

    return result;
}
