#include "Kernel.h"

#include <cstring>
#include <iostream>
#include <sstream>

Kernel::Kernel(PamacAlpmPackage *package)
    : m_packageName(pamac_package_get_name(&package->parent_instance)),
      m_version(pamac_package_get_version(&package->parent_instance))

{
    std::string description(pamac_package_get_desc(&package->parent_instance));

    std::size_t parenthesisStart = description.find_last_of("(");
    std::size_t parenthesisEnd = description.find_last_of(")");
    lts = false;
    recommended = false;

    if (parenthesisStart != std::string::npos
        && parenthesisEnd != std::string::npos) {
        description = description.substr(parenthesisStart,
                                         parenthesisEnd - parenthesisStart);

        lts = description.find("LTS") != std::string::npos;

        recommended = description.find("recommended") != std::string::npos;
    }
    auto version =
        pamac_package_get_installed_version(&package->parent_instance);
    installed = version != nullptr && std::strlen(version) != 0;

    real_time = m_packageName.find("-rt") != std::string::npos;

    experimental = m_version.find("rc") != std::string::npos;

    utsname nm;
    uname(&nm);

    auto thisVersion = versionPair();
    auto bootedVersion = parseVersion(nm.release);

    auto bootedRt = std::string(nm.release).find("-rt") != std::string::npos;

    booted = (thisVersion == bootedVersion && bootedRt == real_time);

    not_supported = false;
}

std::optional<Kernel> Kernel::getFromDatabase(const std::string &kernelName,
                                              const Database &db) {
    auto kernel = pamac_database_get_pkg(db.handle(), kernelName.c_str());

    if (!kernel) { return {}; }

    return Kernel(kernel);
}

std::pair<int, int> Kernel::parseVersion(const std::string &versionstr) {
    auto dotIndex = versionstr.find_first_of('.');
    auto dotIndex2 = versionstr.find_first_of('.', dotIndex + 1);

    std::pair<int, int> result;

    result.first = std::stoi(versionstr.substr(0, dotIndex));
    result.second =
        std::stoi(versionstr.substr(dotIndex + 1, dotIndex2 - dotIndex));

    return result;
}

std::string Kernel::getCurrentKernelName() {
    utsname nm;
    uname(&nm);

    std::ostringstream osstream;

    auto bootedVersion = Kernel::parseVersion(nm.release);

    osstream << "linux" << bootedVersion.first << bootedVersion.second;

    return osstream.str();
}
