#ifndef KERNELSERVICE_H
#define KERNELSERVICE_H
#include "Kernel.h"

#include <sigc++/sigc++.h>
#include <vector>
class KernelService {
public:
    KernelService();

    ~KernelService();

    bool installKernel(const Kernel &k);
    std::vector<Kernel> getKernels();
    bool installKernel(const std::string &packageName);
    bool removeKernel(const std::string &packageName);
    static std::string getChangelog(const std::pair<int, int> &pkgName);
    static std::vector<std::string> getExtraModules(const std::string &pkgName);

    sigc::signal<void(
        const std::string &action, const std::string &status, double progress)>
        actionProgressEmitted;
};

#endif // KERNELSERVICE_H
