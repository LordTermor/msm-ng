#include <filesystem>
#include <fstream>
#include <map>
#include <string>
#include <unordered_set>
#include <vector>
auto nobeep_filename = "/etc/modprobe.d/mcp-nobeep.conf";

bool blacklistSpeaker() {
    std::ofstream stream(nobeep_filename, std::ofstream::out);
    if (!stream.is_open()) { return false; }

    stream << "blacklist pcspkr";

    stream.close();

    system("rmmod pcspkr");
    return true;
}
bool removeSpeakerFromBlacklist() {
    auto result = std::filesystem::remove(nobeep_filename);

    system("modprobe pcspkr");

    return result;
}

bool enableTimeZoneSync() {
}

bool disableTimeZoneSync() {
}

bool enableHWClockInLocal() {
}

bool disableHWClockInLocal() {
}

int main(int argc, char **argv) {
    std::unordered_set<std::string> argumentsSet(argv + 1, argv + argc);

    std::map<std::string, std::string> arguments;

    std::transform(
        argumentsSet.begin(), argumentsSet.end(),
        std::inserter(arguments, arguments.begin()),
        [](const std::string &str) -> std::pair<std::string, std::string> {
            int eqPosition = str.find("=");

            auto first = str.substr(0, eqPosition);
            auto second = str.substr(eqPosition + 1, str.length() - eqPosition);

            return std::pair<std::string, std::string>(first, second);
        });

    if (arguments.find("spkr") != arguments.end()) {
        if (arguments["spkr"] == "1") {
            removeSpeakerFromBlacklist();
        } else {
            blacklistSpeaker();
        }
    }

    if (arguments.find("hwLocal") != arguments.end()) {
        if (arguments["hwLocal"] == "1") {
            enableHWClockInLocal();
        } else {
            disableHWClockInLocal();
        }
    }

    if (arguments.find("tzSync") != arguments.end()) {
        if (arguments["tzSync"] == "1") {
            enableTimeZoneSync();
        } else {
            disableTimeZoneSync();
        }
    }

    return 0;
}
