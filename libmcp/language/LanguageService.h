#ifndef LANGUAGESERVICE_H
#define LANGUAGESERVICE_H
#include "LanguagePackage.h"

#include <sigc++/sigc++.h>
#include <vector>
class LanguageService {
public:
    LanguageService();

    std::vector<LanguagePackage> getLocalizationPackages();
    bool installMissingPackages();

    sigc::signal<void(
        const std::string &action, const std::string &status, double progress)>
        actionProgressEmitted;
};

#endif // LANGUAGESERVICE_H
