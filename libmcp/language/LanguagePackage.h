#ifndef LANGUAGEPACKAGE_H
#define LANGUAGEPACKAGE_H
#include <cstring>
#include <filesystem>
#include <pamac.h>
#include <string>
#include <vector>
struct LanguagePackage {
    LanguagePackage(PamacAlpmPackage *pkg, const std::string &language)
        : packageName(pamac_package_get_name(&pkg->parent_instance)),
          description(pamac_package_get_desc(&pkg->parent_instance)),
          language(language)

    {
        auto version =
            pamac_package_get_installed_version(&pkg->parent_instance);
        installed = (version != nullptr && std::strlen(version) > 0);
    }

    std::string packageName;
    std::vector<std::string> parentPackages;
    std::string description;
    std::string language;
    std::string territory;

    bool installed : 1;
    bool needed : 1;

    std::filesystem::path flagPath() const;
};

inline bool operator==(const LanguagePackage &lh, const LanguagePackage &rh) {
    return lh.packageName == rh.packageName;
}
inline bool operator<(const LanguagePackage &lh, const LanguagePackage &rh) {
    return lh.packageName < rh.packageName;
}

#endif // LANGUAGEPACKAGE_H
