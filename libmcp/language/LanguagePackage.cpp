#include "LanguagePackage.h"

std::filesystem::__cxx11::path LanguagePackage::flagPath() const {
    if (territory == "global") { return ""; }

    return std::string(INSTALL_PREFIX) + "/share/mcp/country-flags/" + territory
           + ".svg";
}
