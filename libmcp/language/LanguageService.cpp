#include "LanguageService.h"

#include "common/Utils.h"
#include "nlohmann/json.hpp"

#include <algorithm>
#include <fstream>
#include <iostream>
#include <set>
using namespace Utils;

static const auto lower = [](char c) { return std::tolower(c); };
static const auto upper = [](char c) { return std::toupper(c); };

std::vector<std::string> mutateLocale(const std::string &language,
                                      const std::string &territory) {
    std::vector<std::string> result;

    std::string lowerLanguage(language), lowerTerritory(territory);
    std::string upperLanguage(language), upperTerritory(territory);

    std::transform(language.begin(), language.end(), lowerLanguage.begin(),
                   lower);
    std::transform(territory.begin(), territory.end(), lowerTerritory.begin(),
                   lower);
    std::transform(territory.begin(), territory.end(), upperTerritory.begin(),
                   upper);

    // Example: firefox-i18n-% -> firefox-i18n-en-US
    result.emplace_back(lowerLanguage + "-" + upperTerritory);
    // Example: firefox-i18n-% -> firefox-i18n-en-us
    result.emplace_back(lowerLanguage + "-" + lowerTerritory);
    // Example: firefox-i18n-% -> firefox-i18n-en_US
    result.emplace_back(lowerLanguage + "_" + upperTerritory);
    // Example: firefox-i18n-% -> firefox-i18n-en_us
    result.emplace_back(lowerLanguage + "_" + lowerTerritory);
    // Example: firefox-i18n-% -> firefox-i18n-en
    result.emplace_back(lowerLanguage);

    return result;
}

std::vector<std::pair<std::string, std::string>> getLocales() {
    std::ifstream file("/etc/locale.gen", std::ios::in);
    std::string locale;
    std::vector<std::pair<std::string, std::string>> localeList;
    while (std::getline(file, locale)) {
        if (locale.length() == 0 || locale.front() == '#') { continue; }

        std::size_t delimiterIndex = locale.find(" ");
        locale = locale.substr(0, delimiterIndex);

        if ((delimiterIndex = locale.find(".")) != std::string::npos) {
            locale = locale.substr(0, delimiterIndex);
        }

        delimiterIndex = locale.find("_");

        auto lng = locale.substr(0, delimiterIndex);
        auto ter =
            locale.substr(delimiterIndex + 1, locale.size() - delimiterIndex);

        localeList.emplace_back(lng, ter);
    }
    file.close();
    return localeList;
}
struct LanguagePackageEntry {
    std::string name;
    std::string l10n_package;
    std::vector<std::string> parent_packages;
};

std::vector<LanguagePackageEntry> getLocalizablePackages() {
    std::vector<LanguagePackageEntry> result;

    std::string name =
        std::string(INSTALL_PREFIX) + "/share/mcp/language_packages.json";
    std::ifstream i(name);
    nlohmann::json j;
    try {
        i >> j;

        for (auto &lngPackageEntry : j["Packages"]) {
            LanguagePackageEntry entry;
            entry.name = lngPackageEntry["name"].get<std::string>();
            entry.l10n_package =
                lngPackageEntry["l10n_package"].get<std::string>();

            auto parents = lngPackageEntry["parent_packages"];
            entry.parent_packages.resize(parents.size());
            std::transform(parents.begin(), parents.end(),
                           entry.parent_packages.begin(), [](auto &package) {
                               return package.template get<std::string>();
                           });

            result.emplace_back(entry);
        }
        return result;
    } catch (const std::exception &e) {
        std::cout << e.what();
        return result;
    }
}

LanguageService::LanguageService() {
    transaction.emit_action_progress.connect([this](const std::string &action,
                                                    const std::string &status,
                                                    double progress) {
        actionProgressEmitted.emit(action, status, progress);
    });

    transaction.emit_hook_progress.connect(
        [this](const std::string &action,
               [[maybe_unused]] const std::string &details,
               const std::string &status, double progress) {
            actionProgressEmitted.emit(action, status, progress);
        });
    transaction.finished.connect([this]([[maybe_unused]] bool success) {
        actionProgressEmitted.emit("finished", "", 1);
    });

    pamac_config_set_simple_install(pamac_database_get_config(db.handle()),
                                    true);
}

template<typename T>
std::string string_transform(const std::string &str, T func) {
    std::string result;
    std::transform(str.begin(), str.end(), std::back_inserter(result), func);
    return result;
}

std::vector<LanguagePackage> LanguageService::getLocalizationPackages() {
    std::set<LanguagePackage> result;

    auto localeList = getLocales();
    auto locPkgs = getLocalizablePackages();
    for (const auto &[lng, ter] : localeList) {
        auto mutations = mutateLocale(lng, ter);
        for (const auto &locPkg : locPkgs) {
            PamacAlpmPackage *pkg = nullptr;
            std::size_t index;
            bool global = false;
            if ((index = locPkg.l10n_package.find("%")) != std::string::npos) {
                for (auto &mutation : mutations) {
                    auto modifiedPkg {locPkg.l10n_package};

                    modifiedPkg.replace(index, 1, mutation);

                    pkg = pamac_database_get_pkg(Utils::db.handle(),
                                                 modifiedPkg.data());

                    if (pkg != nullptr) { break; }
                }
                global = false;

            } else {
                pkg = pamac_database_get_pkg(Utils::db.handle(),
                                             locPkg.l10n_package.data());
                global = true;
            }

            if (pkg != nullptr) {
                LanguagePackage package(pkg, lng);
                package.parentPackages = locPkg.parent_packages;
                package.needed = false;
                package.territory =
                    global ? "global" : string_transform(ter, lower);

                for (auto &pkg : locPkg.parent_packages) {
                    auto installedParent = pamac_database_get_installed_pkg(
                        Utils::db.handle(), pkg.data());

                    if (installedParent != nullptr) {
                        package.needed = true;
                        break;
                    }
                }
                result.emplace(package);
            }
        }
    }
    return std::vector(result.begin(), result.end());
}

bool LanguageService::installMissingPackages() {
    auto pkgs = getLocalizationPackages();
    for (auto &pkg : pkgs) {
        if (pkg.needed && !pkg.installed) {
            pamac_transaction_add_pkg_to_install(transaction.handle(),
                                                 pkg.packageName.data());
        }
    }

    transaction.run();

    return true;
}
