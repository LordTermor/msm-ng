project(libmcp LANGUAGES CXX)

find_package( PythonInterp 3 REQUIRED )

add_library(${PROJECT_NAME}
    SHARED
    kernel/Kernel.cpp
    kernel/Kernel.h
    kernel/KernelService.cpp
    kernel/KernelService.h
    language/LanguagePackage.cpp
    language/LanguagePackage.h
    common/pamacpp/Transaction.cpp
    common/pamacpp/Transaction.h
    common/pamacpp/Database.cpp
    common/pamacpp/Database.h
    common/pamacpp/Package.cpp
    common/pamacpp/Package.h
    common/Utils.cpp
    common/Utils.h
    language/LanguageService.cpp
    language/LanguageService.h
#    hello/HelloService.h
#    hello/HelloService.cpp
#    misc/MiscService.cpp
#    misc/MiscService.h
#    dashboard/SystemIssue.h
#    dashboard/SystemIssue.cpp
#    plasma-layouts/PlasmaLayoutsSwitcherService.h 
#    plasma-layouts/PlasmaLayoutsSwitcherService.cpp
#    plasma-layouts/LayoutItem.h 
#    plasma-layouts/LayoutItem.cpp
    )

target_include_directories(${PROJECT_NAME}
    PUBLIC
    ./
    )

set_target_properties(${PROJECT_NAME} PROPERTIES PREFIX "")

target_compile_definitions(${PROJECT_NAME}
    PRIVATE INSTALL_PREFIX=\"${CMAKE_INSTALL_PREFIX}\")

install(TARGETS ${PROJECT_NAME})
install( FILES ${CMAKE_CURRENT_SOURCE_DIR}/language/language_packages.json DESTINATION share/mcp/ )
install( DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/language/country-flags/svg/ DESTINATION share/mcp/country-flags )

if(CACHE_CHANGELOGS)
    file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/kernel/changelogs/)

    add_custom_target(
     cache_changelogs ALL
     COMMAND ${PYTHON_EXECUTABLE} ${CMAKE_CURRENT_SOURCE_DIR}/kernel/cache_changelog.py
     WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/kernel/changelogs/
     COMMENT "Caching changelogs..."
    )
    install( DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/kernel/changelogs/ DESTINATION share/mcp/changelogs/)

    add_dependencies(${PROJECT_NAME} cache_changelogs)
endif(CACHE_CHANGELOGS)


target_link_libraries(
    ${PROJECT_NAME}
    ${SIGCXX_LIBRARIES}
    ${PAMAC_LIBRARIES}
    ${GLIB_LIBRARIES}
    ${GOBJECT_LIBRARIES}
    ${JSON_LIBRARIES})
