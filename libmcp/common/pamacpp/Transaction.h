#ifndef TRANSACTION_H
#define TRANSACTION_H

#include "Database.h"

#include <pamac.h>
#include <sigc++/sigc++.h>
#include <string>
class Transaction {
public:
    Transaction(const Database &db);

    ~Transaction();

    sigc::signal<void(
        const std::string &action, const std::string &status, double progress)>
        emit_action_progress;

    sigc::signal<void(const std::string &action,
                      const std::string &details,
                      const std::string &status,
                      double progress)>
        emit_hook_progress;

    sigc::signal<void(bool success)> finished;

    PamacTransaction *handle() const;

    void run();

private:
    void _init();
    PamacTransaction *m_handle;
};

#endif // TRANSACTION_H
