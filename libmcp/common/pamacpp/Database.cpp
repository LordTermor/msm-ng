#include "Database.h"

Database::Database(const std::string &configName)
    : m_handle(pamac_database_new(pamac_config_new(configName.data()))) {
}

PamacDatabase *Database::handle() const {
    return m_handle;
}
