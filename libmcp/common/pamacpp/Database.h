#ifndef DATABASE_H
#define DATABASE_H
#include <pamac.h>
#include <string>
class Database {
public:
    Database(const std::string &configName);

    PamacDatabase *handle() const;

private:
    PamacDatabase *m_handle;
};

#endif // DATABASE_H
