﻿#include "Transaction.h"

Transaction::Transaction(const Database &db)
    : m_handle(pamac_transaction_new(db.handle())) {
    _init();
}

Transaction::~Transaction() {
    pamac_transaction_quit_daemon(m_handle);
}

void Transaction::_init() {
    g_signal_connect(m_handle, "emit_action_progress",
                     reinterpret_cast<GCallback>(
                         +[]([[maybe_unused]] GObject *obj, char *action,
                             char *status, double progress, Transaction *self) {
                             self->emit_action_progress.emit(action, status,
                                                             progress);
                         }),
                     this);
    g_signal_connect(
        m_handle, "emit_hook_progress",
        reinterpret_cast<GCallback>(
            +([]([[maybe_unused]] GObject *obj, char *action, char *details,
                 char *status, double progress, Transaction *self) {
                self->emit_hook_progress.emit(action, details, status,
                                              progress);
            })),
        this);
}

PamacTransaction *Transaction::handle() const {
    return m_handle;
}

void Transaction::run() {
    pamac_transaction_run_async(
        m_handle,
        reinterpret_cast<GAsyncReadyCallback>(
            +[]([[maybe_unused]] PamacTransaction *transaction,
                GAsyncResult *result, void *selfPtr) {
                auto res = pamac_transaction_run_finish(transaction, result);

                reinterpret_cast<Transaction *>(selfPtr)->finished.emit(res);
            }),
        this);
}
