#ifndef UTILS_H
#define UTILS_H
#include "pamac.h"

#include <common/pamacpp/Database.h>
#include <common/pamacpp/Transaction.h>
#include <functional>
#include <glib/gslist.h>
#include <memory>
#include <sigc++/sigc++.h>
#include <vector>

namespace Utils {
inline Database db("/etc/mcp/pamac.conf");
inline Transaction transaction(db);

template<typename T>
std::vector<T> gsListToVector(GSList *list,
                              std::function<T(void *)> conversion) {
    std::vector<T> result;
    for (auto &it = list; it != nullptr; it = it->next) {
        result.emplace_back(conversion(it->data));
    }
    return result;
}
template<typename T>
std::vector<T> gListToVector(GList *list, std::function<T(void *)> conversion) {
    std::vector<T> result;
    for (auto &it = list; it != nullptr; it = it->next) {
        result.emplace_back(conversion(it->data));
    }
    return result;
}

template<typename T, typename TFun>
std::vector<T> gPtrArrayToVector(GPtrArray *arr, TFun conversion) {
    std::vector<T> result;

    std::pair p(&result, conversion);
    g_ptr_array_foreach(
        arr,
        [](void *dataPtr, void *selfData) {
            auto pair = reinterpret_cast<
                std::pair<std::vector<T> *, std::function<T(void *)>> *>(
                selfData);
            pair->first->emplace_back(pair->second(dataPtr));
        },
        &p);

    return result;
}

template<typename T>
using ViewPtr = std::unique_ptr<T, std::function<void(T *)>>;

}; // namespace Utils

#endif // UTILS_H
