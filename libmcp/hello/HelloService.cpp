#include "HelloService.h"

#include <fstream>
#include <nlohmann/json.hpp>

std::vector<HelloEntry>
    HelloService::parseHelloEntriesJson(const std::string &fileName) {
    std::vector<HelloEntry> result;
    std::ifstream i(fileName);

    nlohmann::json j;
    i >> j;

    result.resize(j.size());

    std::transform(j.begin(), j.end(), result.begin(),
                   [this](const nlohmann::json &entryJson) {
                       HelloEntry res;

                       res.name = entryJson["name"].get<std::string>();
                       res.iconUrl = entryJson["iconUrl"].get<std::string>();

                       auto type = entryJson["type"].get<std::string>();
                       auto data = entryJson["data"].get<std::string>();

                       res.handler = [this, type, data]() {
                           typeToHandler[type](data);
                       };

                       return res;
                   });
    return result;
}

HelloService::HelloService() {
    typeToHandler = {
        {"external",
         [](const std::string &data) { system(("xdg-open " + data).c_str()); }},
        {"text",
         [this](const std::string &data) { showPageRequested.emit(data); }},
        {"file", [this](const std::string &filename) {
             std::ifstream stream(filename);
             std::string text;
             stream >> text;
             stream.close();

             showPageRequested.emit(text);
         }}};

    m_entries = parseHelloEntriesJson(std::string(INSTALL_PREFIX)
                                      + "/share/mcp/hello_entries.json");
}
