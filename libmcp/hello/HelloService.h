#ifndef HELLOSERVICE_H
#define HELLOSERVICE_H

#include <functional>
#include <map>
#include <sigc++/sigc++.h>
#include <string>
#include <vector>

struct HelloEntry {
    std::string name;
    std::string iconUrl;

    std::function<void()> handler;
};

class HelloService {
public:
    HelloService();

    std::vector<HelloEntry> entries() { return m_entries; }

    sigc::signal<void(std::string)> showPageRequested;

private:
    std::vector<HelloEntry> parseHelloEntriesJson(const std::string &fileName);
    std::map<std::string, std::function<void(const std::string &)>>
        typeToHandler;
    std::vector<HelloEntry> m_entries;
};

#endif // HELLOSERVICE_H
