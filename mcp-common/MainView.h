#ifndef MAINVIEW_H
#define MAINVIEW_H
#include <AbstractView.h>

class MainView {
public:
    MainView() = default;
    virtual ~MainView() = default;

    virtual void addView(AbstractView *view, const MetaInfo &info) = 0;
    virtual void _show() = 0;
};

#endif // MAINVIEW_H
