#pragma once

#include <AbstractPresenter.h>
#include <AbstractView.h>
#include <QWidget>
#include <memory>

namespace MCP {

template<typename TView>
class LegacyPresenter : public AbstractPresenter<TView> {
public:
    explicit LegacyPresenter(
        std::unique_ptr<TView, std::function<void(AbstractView *)>> view)
        : AbstractPresenter<TView>(std::move(view)) {
        AbstractPresenter<TView>::m_view->_init();
    }
};

} // namespace MCP
