#pragma once
#include "DashboardView.h"

#include <AbstractPresenter.h>
#include <common/Utils.h>

namespace MCP {

class DashboardPresenter : public AbstractPresenter<DashboardView> {
public:
    DashboardPresenter(Utils::ViewPtr<DashboardView> view);
};

} // namespace MCP
