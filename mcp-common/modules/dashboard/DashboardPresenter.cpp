#include "DashboardPresenter.h"

namespace MCP {

DashboardPresenter::DashboardPresenter(Utils::ViewPtr<DashboardView> view)
    : AbstractPresenter<DashboardView>(std::move(view)) {
}

} // namespace MCP
