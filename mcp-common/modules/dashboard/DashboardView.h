#pragma once

#include <AbstractView.h>
#include <sigc++/signal.h>

namespace MCP {

class DashboardView : public AbstractView {
public:
    DashboardView();

    sigc::signal<void> systemIssuesRequested;

    virtual void setSystemIssues() = 0;
};

} // namespace MCP
