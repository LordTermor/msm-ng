#ifndef MISCPRESENTER_H
#define MISCPRESENTER_H

#include <AbstractPresenter.h>
#include <functional>
#include <memory>
#include <misc/MiscService.h>
#include <modules/misc/MiscAbstractView.h>

class MiscPresenter : public AbstractPresenter<MiscAbstractView> {
public:
    MiscPresenter(std::unique_ptr<MiscAbstractView,
                                  std::function<void(AbstractView *)>> view);

private:
    MiscService m_model;
};

#endif // MISCPRESENTER_H
