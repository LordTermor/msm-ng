#ifndef MISCABSTRACTVIEW_H
#define MISCABSTRACTVIEW_H
#include <AbstractView.h>
#include <any>
#include <map>
#include <sigc++/sigc++.h>
#include <string>
class MiscAbstractView : public AbstractView {
public:
    MiscAbstractView() = default;
    virtual ~MiscAbstractView() = default;

    sigc::signal<void(bool)> changeSpeakerEnabledRequested;
    sigc::signal<void(bool)> changeTimeZoneSyncRequested;
    sigc::signal<void(bool)> changeHWClockInLocalRequested;

    sigc::signal<void> saveRequested;

    sigc::signal<void> settingsValuesRequested;

    virtual void
        setSettingsValues(const std::map<std::string, std::any> &values) = 0;
};

#endif // MISCABSTRACTVIEW_H
