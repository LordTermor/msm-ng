#include "MiscPresenter.h"

MiscPresenter::MiscPresenter(
    std::unique_ptr<MiscAbstractView, std::function<void(AbstractView *)>> view)
    : AbstractPresenter<MiscAbstractView>(std::move(view)) {
    m_view->settingsValuesRequested.connect([this]() {
        bool speakerEditable = false;

        m_view->setSettingsValues(
            {{"spkr", m_model.getPCSpeakerEnabled(&speakerEditable)},
             {"spkrEditable", speakerEditable},
             {"hwLocal", m_model.getHWClockInLocal()},
             {"tzSync", m_model.getTimeZoneSync()}});
    });
    m_view->changeSpeakerEnabledRequested.connect(
        sigc::mem_fun(m_model, &MiscService::setPCSpeakerEnabled));
    m_view->saveRequested.connect([this]() {
        m_model.save();
        bool speakerEditable = false;

        m_view->setSettingsValues(
            {{"spkr", m_model.getPCSpeakerEnabled(&speakerEditable)},
             {"spkrEditable", speakerEditable},
             {"hwLocal", m_model.getHWClockInLocal()},
             {"tzSync", m_model.getTimeZoneSync()}});
    });

    m_view->_init();
}
