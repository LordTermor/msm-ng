#ifndef KERNELABSTRACTVIEW_H
#define KERNELABSTRACTVIEW_H

#include "AbstractView.h"
#include "kernel/Kernel.h"
#include "sigc++/sigc++.h"

#include <string>
#include <vector>

class KernelAbstractView : public AbstractView {
public:
    virtual ~KernelAbstractView() = default;
    sigc::signal<void> kernelListRequested;
    sigc::signal<void(const std::string &)> installKernelRequested;
    sigc::signal<void(const std::string &)> removeKernelRequested;
    sigc::signal<void(const std::string &)> extraModulesRequested;

    virtual void setKernelList(const std::vector<Kernel> &list) = 0;
    virtual void setKernelProgress(const std::string &action,
                                   const std::string &status,
                                   double progress) = 0;
    virtual void setExtraModules(const std::vector<std::string> &modules) = 0;
};

#endif // KERNELABSTRACTVIEW_H
