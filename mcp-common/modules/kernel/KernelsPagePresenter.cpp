#include "KernelsPagePresenter.h"

KernelsPagePresenter::KernelsPagePresenter(
    std::unique_ptr<KernelAbstractView, std::function<void(AbstractView *)>>
        view)
    : AbstractPresenter<KernelAbstractView>(std::move(view)) {
    m_view->kernelListRequested.connect(
        [this]() { m_view->setKernelList(m_model.getKernels()); });
    m_view->installKernelRequested.connect(
        [this](const std::string &pkgname) { m_model.installKernel(pkgname); });

    m_view->removeKernelRequested.connect(
        [this](const std::string &pkgname) { m_model.removeKernel(pkgname); });

    m_view->extraModulesRequested.connect(
        [this](const std::string &kernelname) {
            m_view->setExtraModules(m_model.getExtraModules(kernelname));
        });

    m_model.actionProgressEmitted.connect([this](const std::string &action,
                                                 const std::string &status,
                                                 double progress) {
        m_view->setKernelProgress(action, status, progress);
    });

    m_view->_init();
}
