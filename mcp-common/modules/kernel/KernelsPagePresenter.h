#ifndef KERNELSPAGEPRESENTER_H
#define KERNELSPAGEPRESENTER_H
#include "AbstractPresenter.h"
#include "KernelAbstractView.h"
#include "kernel/KernelService.h"

#include <memory>
class KernelsPagePresenter : public AbstractPresenter<KernelAbstractView> {
public:
    KernelsPagePresenter(
        std::unique_ptr<KernelAbstractView, std::function<void(AbstractView *)>>
            view);

private:
    KernelService m_model;
};

#endif // KERNELSPAGEPRESENTER_H
