#include "LanguagePacksPagePresenter.h"

LanguagePacksPagePresenter::LanguagePacksPagePresenter(
    std::unique_ptr<LanguagePackAbstractView,
                    std::function<void(AbstractView *)>> view)
    : AbstractPresenter(std::move(view)) {
    m_view->packagesRequested.connect([this]() {
        m_view->setPackageList(m_model.getLocalizationPackages());
    });

    m_view->installMissingRequested.connect(
        [this]() { m_model.installMissingPackages(); });

    m_model.actionProgressEmitted.connect([this](const std::string &action,
                                                 const std::string &status,
                                                 double progress) {
        m_view->setProgress(action, status, progress);
    });

    m_view->_init();
}
