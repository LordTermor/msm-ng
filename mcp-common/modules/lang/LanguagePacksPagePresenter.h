#ifndef LANGUAGEPACKSPAGEPRESENTER_H
#define LANGUAGEPACKSPAGEPRESENTER_H
#include "AbstractPresenter.h"
#include "LanguagePackAbstractView.h"
#include "language/LanguageService.h"

#include <functional>
#include <memory>
class LanguagePacksPagePresenter
    : public AbstractPresenter<LanguagePackAbstractView> {
public:
    LanguagePacksPagePresenter(
        std::unique_ptr<LanguagePackAbstractView,
                        std::function<void(AbstractView *)>> view);

private:
    LanguageService m_model;
};

#endif // LANGUAGEPACKSPAGEPRESENTER_H
