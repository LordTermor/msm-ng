#ifndef LANGUAGEPACKABSTRACTVIEW_H
#define LANGUAGEPACKABSTRACTVIEW_H
#include "AbstractView.h"

#include <language/LanguagePackage.h>
#include <sigc++/signal.h>
#include <vector>
class LanguagePackAbstractView : public virtual AbstractView {
public:
    LanguagePackAbstractView() = default;
    virtual ~LanguagePackAbstractView() = default;

    sigc::signal<void> packagesRequested;
    sigc::signal<void> installMissingRequested;
    virtual void setPackageList(const std::vector<LanguagePackage> &) = 0;
    virtual void setProgress(const std::string &action,
                             const std::string &status,
                             double progress) = 0;
};

#endif // LANGUAGEPACKABSTRACTVIEW_H
