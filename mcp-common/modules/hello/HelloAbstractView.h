#ifndef HELLOABSTRACTVIEW_H
#define HELLOABSTRACTVIEW_H
#include "AbstractView.h"

#include <hello/HelloService.h>
#include <sigc++/sigc++.h>
class HelloAbstractView : public AbstractView {
public:
    HelloAbstractView();
    virtual ~HelloAbstractView() = default;

    sigc::signal<void> entriesRequested;

    virtual void setEntries(const std::vector<HelloEntry> &entries) = 0;
};

#endif // HELLOABSTRACTVIEW_H
