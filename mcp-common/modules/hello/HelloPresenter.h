#ifndef HELLOPRESENTER_H
#define HELLOPRESENTER_H
#include "AbstractPresenter.h"
#include "HelloAbstractView.h"

#include <functional>
#include <hello/HelloService.h>
#include <memory>

class HelloPresenter : public AbstractPresenter<HelloAbstractView> {
public:
    HelloPresenter(std::unique_ptr<HelloAbstractView,
                                   std::function<void(AbstractView *)>> view);

private:
    HelloService m_service;
};

#endif // HELLOPRESENTER_H
