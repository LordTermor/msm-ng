#include "HelloPresenter.h"

HelloPresenter::HelloPresenter(
    std::unique_ptr<HelloAbstractView, std::function<void(AbstractView *)>>
        view)
    : AbstractPresenter<HelloAbstractView>(std::move(view)) {
    m_view->entriesRequested.connect(
        [this]() { m_view->setEntries(m_service.entries()); });

    m_view->_init();
}
