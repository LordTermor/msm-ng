#ifndef MHWDPRESENTER_H
#define MHWDPRESENTER_H

#include <AbstractPresenter.h>

namespace MCP {

class MhwdPresenter : public AbstractPresenter<AbstractView> {
public:
    MhwdPresenter();
};

} // namespace MCP

#endif // MHWDPRESENTER_H
