#ifndef ABSTRACTVIEW_H
#define ABSTRACTVIEW_H
#include <string>
struct MetaInfo {
    std::string name;
    std::string description;
    std::string iconName;
};

class AbstractView {
public:
    AbstractView() = default;
    virtual ~AbstractView() = default;

    virtual void _show() = 0;

    // will be called after presenter's work
    virtual void _init() = 0;
};

#endif // ABSTRACTVIEW_H
