#ifndef ABSTRACTPRESENTER_H
#define ABSTRACTPRESENTER_H

#include "AbstractView.h"

#include <functional>
#include <memory>

class AbstractPresenterBase {
public:
    virtual ~AbstractPresenterBase() = default;
};

template<typename T> class AbstractPresenter : public AbstractPresenterBase {
public:
    AbstractPresenter(
        std::unique_ptr<T, std::function<void(AbstractView *)>> view)
        : m_view(std::move(view)) {
        static_assert(
            std::is_base_of<AbstractView, T>::value,
            "type parameter of this class must derive from BaseClass");
    };
    virtual ~AbstractPresenter() = default;

    virtual AbstractView *getView() { return m_view.get(); }

protected:
    std::unique_ptr<T, std::function<void(AbstractView *)>> m_view;
};

#endif // ABSTRACTPRESENTER_H
