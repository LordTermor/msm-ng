#ifndef MAINPRESENTER_H
#define MAINPRESENTER_H
#include "AbstractPresenter.h"

#include <MainView.h>
#include <memory>
#include <vector>
class MainPresenter {
public:
    MainPresenter(MainView *view);

    template<typename T>
    void addPresenter(AbstractPresenter<T> *presenter,
                      const MetaInfo &info = MetaInfo()) {
        m_presenters.emplace_back(presenter);
        m_viewMain->addView(presenter->getView(), info);
    }

private:
    std::vector<AbstractPresenterBase *> m_presenters;
    std::unique_ptr<MainView> m_viewMain;
};

#endif // MAINPRESENTER_H
